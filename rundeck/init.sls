{% set env = grains['environment'] %}

rundeck:
  db_image:
    version: 10.3.2
    db_name: rundeckdb
    user: rundeck
  disk:
    device_id: 'xvdb'
    partition: 'opt/containers'
  rundeck_image:
    name: "jordan/rundeck"
    version: "2.10.8"
    login_module: "RDpropertyfilelogin"
    external_server_url: "rundeck-{{ env }}-aavnict.aavn.local"
{% if env == 'dev' %}
    id_rsa.pub: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDT8+dvidraVXg0q584qz63wyr7kRlyiaFiy7/E/6dtOUNWsroKdCb7EyL4YnKbo9Sot0dioYLE+/YorBd5TGZ2kyAipD8rpO5Xe6sAWhDcYU9tSXXLWfHG2ZbFHvnYx6yD9033D+/kdPu9KUU7dCnQEcyt7Y3qlNf41N5qEXPEF0/VyWeateI+BOhH5orA2cJqfcW6xvh/udlrpNJv57aHFW+7VdDWClTIvfmzOZL4jtV0iI6tO7rDa9ox+Ezlg5vH5JTnWiYDEqnA8WPVcYD1ETJ9GLnZUl/Hxuwo9TGX/OCkX+zRzSN8Qt2xwHigvCIZTeTWXZA8Ge1P4RzTlIL7M/j9YrPOl0wv5y4UXAhChsQ53R4BFIYdJhVvJDxM4O9Yd+msBpxk1li+WwM+l3UyzNJbk3K+7My7J9bF1D8hkZuziio9iz4F0Wn17znkrPlUSB6oT1AE+14uFX4i1IFNpOiB9ksTQQ0br3cQM8GQrIo+pGW6rUpzxa1jWGLcHW1TyDh0cFHRTPinRE2HghbZ1hb+bCjO6JZRQkW+H/yODfiCz36kDfpM+ImHKDFXg0+rdx2WSp+N6hblmpZ5Tz1MNuz07WL6zRBRmqr1RJ6dO9Zm4qPhZfOcxEN0wIt5R/FrXh42RWyDOH+hvznwMVuIZ4tfH98mXzdLFbLvXfQRMQ== tin.tranvan@axonactive.com"
{% elif env == 'prod' %}
    id_rsa.pub: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDT8+dvidraVXg0q584qz63wyr7kRlyiaFiy7/E/6dtOUNWsroKdCb7EyL4YnKbo9Sot0dioYLE+/YorBd5TGZ2kyAipD8rpO5Xe6sAWhDcYU9tSXXLWfHG2ZbFHvnYx6yD9033D+/kdPu9KUU7dCnQEcyt7Y3qlNf41N5qEXPEF0/VyWeateI+BOhH5orA2cJqfcW6xvh/udlrpNJv57aHFW+7VdDWClTIvfmzOZL4jtV0iI6tO7rDa9ox+Ezlg5vH5JTnWiYDEqnA8WPVcYD1ETJ9GLnZUl/Hxuwo9TGX/OCkX+zRzSN8Qt2xwHigvCIZTeTWXZA8Ge1P4RzTlIL7M/j9YrPOl0wv5y4UXAhChsQ53R4BFIYdJhVvJDxM4O9Yd+msBpxk1li+WwM+l3UyzNJbk3K+7My7J9bF1D8hkZuziio9iz4F0Wn17znkrPlUSB6oT1AE+14uFX4i1IFNpOiB9ksTQQ0br3cQM8GQrIo+pGW6rUpzxa1jWGLcHW1TyDh0cFHRTPinRE2HghbZ1hb+bCjO6JZRQkW+H/yODfiCz36kDfpM+ImHKDFXg0+rdx2WSp+N6hblmpZ5Tz1MNuz07WL6zRBRmqr1RJ6dO9Zm4qPhZfOcxEN0wIt5R/FrXh42RWyDOH+hvznwMVuIZ4tfH98mXzdLFbLvXfQRMQ== tin.tranvan@axonactive.com"
{% endif %}
