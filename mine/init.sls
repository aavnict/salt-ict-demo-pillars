mine_functions:
  mine_grains_items:
    mine_function: grains.items
  mine_ip_addrs:
    mine_function: network.ip_addrs
    cidr: 10.133.0.0/24