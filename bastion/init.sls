bastion:
  config:
    log_dir: "/var/log/bastion/"
    ssh_port: '992'
    groups:
          name: "fowarding-group"
          permit_open: "*:80 *:443 *:8080 *:5432 *:9200 *:9300 *:2375 *:2376 *:4440"

  s3:
    host_base: "s3.amazonaws.com"
    host_bucket: "%(bucket)s.s3.amazonaws.com"
    bucket_location:  "US"
    bucket: "ict-demo.aavn.local"
    users_path: "bastion/users"
    acl_path: "bastion/acls"
    logs_path: "bastion/logs"