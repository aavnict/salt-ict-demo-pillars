
#########################################################
# ACL variables per ENV. Add or remove IPs just here    #
#########################################################

{% set dev_allowed_ips = [
'10.148.10.0/24',
'194.209.29.2',
'127.0.0.1',
'0.0.0.0/0'
]
%}


{% set prod_allowed_ips = [
'10.109.10.0/24',
'194.209.29.2'
]
%}

{% set proxy_headers = [
'proxy_set_header Host $host',
'proxy_set_header X-Real-IP $remote_addr',
'proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for',
'proxy_set_header X-Forwarded-Proto $scheme',
'proxy_set_header X-Forwarded-Host $host',
'proxy_set_header X-Forwarded-Server $host',
'proxy_set_header X-Forwarded-Port $server_port',
]
%}

{% set env = grains['environment'] %}

nginx:
  server:
    pkgs:
      - nginx
      - httpd-tools
    nginx_conf:
      worker:
        processes: "{{ salt['grains.get']('num_cpus','auto') }}"
        connections: "1024"
    proxy_header:
      header_file_path: /etc/nginx/header.conf
      header_list: {{ proxy_headers }}
    upstreams:
      rundeck-upstream:
        servers:
{% set mine_ip_addrs = salt.saltutil.runner('mine.get',
  tgt='*rundeck*',
  tgt_type='compound',
  fun='mine_ip_addrs') %}
{% for minion_id, ip_addr in mine_ip_addrs.items() %}
          - "{{ ip_addr[0] }}:8080"
{% endfor %}
          - 172.31.4.132:8080 weight=5
      tomcat_upstream:
        servers:
          - 172.31.4.132:8080 weight=5

    sites:
      nginx_proxy_rundeck:
        type: nginx_proxy
        ssl:
          key_file: /etc/ssl/private/axi-net.key
          cert_file: /etc/ssl/certs/axi-net.crt
          fullchain: /etc/ssl/certs/ocsp-chain.crt
        access_policy:
{% if grains['environment'] == 'dev' %}
          allowed_ips: {{ dev_allowed_ips }}
{% elif grains['environment'] == 'prod' %}
          allowed_ips: {{ prod_allowed_ips }}
{% endif %}
        proxy:
          headers_path: /etc/nginx/header.conf
          upstream_proxy_pass: rundeck-upstream
          protocol: http
        host:
          name: rundeck-{{ env }}-aavnict.aavn.local
          listen: "443 ssl http2"



##########################################################
# HTTP and Stats Server. For port redirection to HTTPS   #
##########################################################
      nginx_stats_server:
        enabled: true
        type: nginx_stats
        name: stats_server
        host:
          name: rproxy-axi-dpp-{{ env }}-s1s.axoninsight.net
          listen: 8080


ssl_certificate:
  key:
    path: /etc/ssl/private/axi-net.key
  cert:
    path: /etc/ssl/certs/axi-net.crt
  fullchain:
    path: /etc/ssl/certs/ocsp-chain.crt
