# Pillar Top file
base:
  '*':
    - mine.init
    - common.init
    - common.secrets
    - bastion.init
    - bastion.secrets
  '*rundeck*':
    - docker.init
    - docker.secrets
    - rundeck.init
    - rundeck.secrets
  '*db*':
    - postgresql.init
    - postgresql.secrets
  '*proxy*':
    - nginx.init
    - nginx.secrets
